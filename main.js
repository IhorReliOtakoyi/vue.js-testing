Vue.component('appinput', {
  props: ['name', 'value', 'pattern', 'type', 'id', 'idinput'],
  template: `
            <div :id="id" class="form-group form-focus">
                <label class="control-label">{{name}}</label>
                <span class="fa" :class="validClass" v-if="activated">
                    </span>
                <input class="form-control floating"
                       :type="type"
                       :id="'check' + idinput"
                       :value="value"
                       @input="onInput"
                />

            </div>
      `,
  data() {
    return {
      activated: this.value != ''
    }
  },
  computed: {
    validClass() {
      return this.pattern.test(this.value) ?
        'fa-check-circle text-success' :
        'fa-exclamation-circle text-danger'
    }
  },
  methods: {
    onInput(e) {
      this.activated = true;

      this.$emit('changedata', {
        value: e.target.value,
        valid: this.pattern.test(e.target.value)
      });
    },
  }
});


Vue.component('app-account', {
  props: ['loginHave'],
  template: `
    <div @click="accountShow()" class="text-center accountButton">
                <a href="#">Already have an account?</a>
            </div>
    `,
  data() {

  },
  methods: {
    accountShow() {
      $('.form1').css('display', 'none');
      $('.container-account').css('display', 'block');
      $('.accountButton').css('display', 'none');
    }
  }
});


new Vue({
  el: '.app',
  data: {
    title: 'Register',
    loggedIn: false,
    done: 0,
    isEditing: false,
    info: [
      {
        name: 'Username',
        value: '',
        pattern: /^[a-zA-Z ]{2,30}$/,
        type: 'text',
        id: 'nameId',
        idinput: 1,
        editid: 'editName'
      },
      {
        name: 'Email',
        value: '',
        pattern: /[A-Za-z0-9._%+-]{3,}@[a-zA-Z]{3,}([.]{1}[a-zA-Z]{2,}|[.]{1}[a-zA-Z]{2,}[.]{1}[a-zA-Z]{2,})/,
        type: 'email',
        id: 'emailId',
        idinput: 2
      },
      {
        name: 'Password',
        value: '',
        pattern: /.+/,
        type: 'password',
        id: 'passwordId',
        idinput: 3,
        editid: 'editPass'
      },
      {
        name: 'Repeat Password',
        value: '',
        pattern: /.+/,
        type: 'password',
        id: 'repeatPasswordId',
        idinput: 4,
        displaynone: 'displayNone'

      }
    ],
    controls: [],
    checkname: '',
    checkpass: ''
  },
  beforeMount() {
    for (var i = 0; i < this.info.length; i++) {
      this.controls.push(false);
    }
  },
  methods: {
    onChangeData(index, data) {
      this.info[index].value = data.value;
      this.controls[index] = data.valid;

      var done = 0;
      var check3 = document.getElementById('check3');
      var check4 = document.getElementById('check4');
      var passwordError = document.querySelector('#passwordId span');
      var passwordRepeatError = document.querySelector('#repeatPasswordId span');

      for (var i = 0; i < this.controls.length; i++) {
        if (this.controls[i]) {
          done++;
        }
      }

      this.done = done;

      if (check3.value ==
        check4.value) {
        passwordError.className = 'fa fa-check-circle text-success';
        passwordRepeatError.className = 'fa fa-check-circle text-success';
      } else {
        passwordError.className = 'fa fa-exclamation-circle text-danger';
        passwordRepeatError.className = 'fa fa-exclamation-circle text-danger';
      }
    },
    recordLogin() {
      if (loggedIn = true) {
        $('.accountButton').css('display', 'none');
      }

      var data = [{
        "name": $('#check1').val(),
        "password": $('#check3').val(),
      }]

      var saveData = JSON.stringify(data);

      if (typeof(Storage) !== undefined) {
        localStorage.setItem("users", saveData);
      }
    },
    checkLogin() {
      var user = JSON.parse(localStorage.getItem("users"));
      var nameHtml = this.checkname;
      var namePass = this.checkpass;

      if (user[0].name == nameHtml && user[0].password == namePass) {
        localStorage.setItem("user", nameHtml);
        $('.container-account').css('display', 'none');
        $('.account-success').css('display', 'block');
      } else {
        $('.noaccount').css('display', 'block');
      }
    },
    editContent() {
      if (!this.isEditing) {
        this.isEditing = !this.isEditing;
      }
    },
    saveContent() {
      var data = [{
        "name": $('#editName').val(),
        "password": $('#editPass').val(),
      }]

      var saveData = JSON.stringify(data);


      localStorage.setItem("users", saveData);

      if (this.isEditing) {
        this.isEditing = !this.isEditing;
      }
    },
    reset(index, value) {
      for (var i = 0; i < this.info.length; i++) {
        this.info[i].value = '';
      }

      this.loggedIn = false;
      var el = document.querySelector('.accountButton');
      el.style.display = 'block';
    },
    logout() {
      this.reset();
    },
  }
});
